var config = {
    userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
}

module.exports = {
    getConfig: function () {
        return config;
    }
}
