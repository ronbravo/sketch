#!/usr/bin/env node
var express = require ('express');
var app = express ();


var config = require ('./sys/config');
var port = 3000;

app.get ('/', function (req, res) {
    res.send ('Sketch Frontend Proxy Server');
});

app.all ('/site/:siteUrl*', function (req, res) {
    res.send ('accessing a specific site.');
});

app.listen (port, function () {
    console.log ('- Sketch is now running on port: ', port);
});
